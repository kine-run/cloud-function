const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

/**
 * Triggers when a user gets a new follower and sends a notification.
 *
 * Followers add a flag to `/followers/{followedUid}/{followerUid}`.
 * Users save their device notification tokens to `/users/{followedUid}/notificationTokens/{notificationToken}`.
 */
exports.sendEndRunNotification = functions.firestore
  .document('users/{userId}/runs/{runId}')
  .onCreate(async (snapshot, context) => {
    console.log(snapshot, context);
    const user = await admin.firestore().collection('users').doc(context.params.userId).get();
    console.log(user);
    await admin.messaging().sendToDevice(
      user.data().token,
      {
        notification: {
          title: 'Great !',
          body: 'Have a nice run',
        },
      },
      {
        contentAvailable: true,
        priority: 'high',
      }
    );
  });
